# IBM Data Science Specialization Capstone

Capstone for the IBM DS specialization on Coursera, using Jupyter notebooks for code and comments as required.

Final deliverables:

* `carRideShareAnalysisPres.pdf`: presentation style (short)
* `carRideShareAnalysisReport.pdf`: detail report

